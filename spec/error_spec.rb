require 'spec_helper'
require 'gosms_client'

describe GosmsClient::Error do

  describe "Basic functionality" do
    it "can print details" do
      err = GosmsClient::Error.new(:reason, "something")
      expect(err.to_s).to eql "reason: something"
    end

    it "can handle no details provided" do
      err = GosmsClient::Error.new(:other_reason)
      expect(err.to_s).to eql "other_reason"
    end

    it "double nil is acceptable" do
      err = GosmsClient::Error.new(nil, nil)
      expect(err.to_s).to eql "general"
    end

    it "can handle hash in details" do
      hash = {
        "status" => 400,
        "detail" => "Something bad",
        "errors" => "Some errors"}
      err = GosmsClient::Error.new(:reason, hash)
      expect(err.to_s).to eql "reason: #{hash.to_s}"
    end

    it "can handle json in details" do
      json = {
        "status" => 400,
        "detail" => "Something bad",
        "errors" => "Some errors"}.to_json
      err = GosmsClient::Error.new(:reason, json)
      expect(err.to_s).to eql "reason: #{json.to_s}"
    end
    
  end

  describe "Detail" do

    it "handles symbols as strings" do
      err = GosmsClient::Error.new(:reason, :details)
      expect(err.to_s).to eql "reason: details"
    end

    it "handles arrays" do
      err = GosmsClient::Error.new(:reason, [])
      expect(err.to_s).to eql "reason: []"
    end

    it "handles integers" do
      err = GosmsClient::Error.new(:reason, 123456)
      expect(err.to_s).to eql "reason: 123456"
    end

    it "explicit nil is allowed" do
      err = GosmsClient::Error.new(:reason, nil)
      expect(err.to_s).to eql "reason"
    end

  end

  describe "Reason" do
    it "handles strings" do
      err = GosmsClient::Error.new("reason")
      expect(err.to_s).to eql "reason"
    end

    it "handles numbers" do
      err = GosmsClient::Error.new(123456)
      expect(err.to_s).to eql "123456"
    end

    it "nil is considered as general error" do
      err = GosmsClient::Error.new(nil)
      expect(err.to_s).to eql "general"
    end
  end
end

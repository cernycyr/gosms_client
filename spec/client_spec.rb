require 'spec_helper'
require 'gosms_client'

describe GosmsClient::Client do

  before do
    @client = GosmsClient::Client.new("id","secret","channel")
    stub_auth
  end

  it "authenticates on first request" do
    stub_request(:get, "https://app.gosms.cz/api/v1/").
      to_return(:status => 200,
                :body => "",
                :headers => {"Content-Type"=> "text/plain"})

    2.times {@client.get_credit}
    expect(WebMock).to have_requested(:get, "https://app.gosms.cz/api/v1/").twice
    expect(WebMock).to have_requested(:post, "https://app.gosms.cz/oauth/v2/token").
       with(:body => hash_including({
         "client_id"=>"id",
         "client_secret"=>"secret",
         "grant_type"=>"client_credentials"})).once
  end

  it "asks for credit" do
    stub_request(:get, "https://app.gosms.cz/api/v1/").
        to_return(:status => 200,
                  :body => {"currentCredit": 231}.to_json,
                  :headers => {"Content-Type"=> "application/json"})
    expect(@client.get_credit).to eql(231)
    expect_requested(:get, "https://app.gosms.cz/api/v1/")
  end

  describe "Sending SMS" do
    it "gets testing info" do
      stub_request(:post, "https://app.gosms.cz/api/v1/messages/test").
           to_return(:status => 200,
                     :body => test_info_body("Hello"),
                     :headers => {"Content-Type"=> "application/json"})

      result = @client.test_send "Hello", 123456789
      expect_requested(:post, "https://app.gosms.cz/api/v1/messages/test")
      expect(result.price).to eql(1.452)
      expect(result.is_test).to eql(true)
    end

    it "will send sms" do
      stub_request(:post, "https://app.gosms.cz/api/v1/messages").
           to_return(:status => 201,
                     :body => {
                       "recipients": {
                         "invalid": [] },
                       "link": "/api/v1/messages/42"}.to_json,
                     :headers => {"Content-Type"=> "application/json"})
      response = @client.send("Hello", "123456789")
      expect_requested(:post, "https://app.gosms.cz/api/v1/messages")
      expect(response.link).to eq("/api/v1/messages/42")
      expect(response.is_test).to eql(false)
    end
  end

  describe "General errors" do
    [400,401,403,404,500].each do |code|
      it "will throw exception on #{code}" do
        stub_error(code)
        expect { @client.test_send "Hello", "123456789" }.
          to raise_error(GosmsClient::HTTPError, "go_sms: An error #{code} has occurred.")
      end
    end
  end

  describe "Specific GoSmS errors (from API documentation)" do
    it "\"wrong channel\" error" do
      json_err = { "type": "", "title": "Non-existent channel", "status": 400,
        "detail": "Channel '12345' doesn't exist in your organization"}.to_json
      stub_error(400, json_err, "application/problem+json")

      expect { @client.test_send "Hello", "123456789" }.
        to raise_error(GosmsClient::HTTPError, "Non-existent channel: Channel '12345' doesn't exist in your organization")
    end

    it "failed to send message" do
      json_err = { "type":"", "title":"Invalid Message", "status":400,
        "detail":"Message contains invalid fields and cannot be sent",
        "errors":[
            "Invalid Recipients: hello,4207396asdfasdfasf",
            "Message cannot be scheduled back in time. Send start date must be greater than current date 2014-09-11T07:20:21+0200" ]}.to_json
      stub_error(400, json_err, "application/problem+json")

      expect { @client.test_send "Hello", "123456789" }.
        to raise_error(GosmsClient::HTTPError, "Invalid Message: Message contains invalid fields and cannot be sent. Errors: [\"Invalid Recipients: hello,4207396asdfasdfasf\", \"Message cannot be scheduled back in time. Send start date must be greater than current date 2014-09-11T07:20:21+0200\"]")
    end

  end

end

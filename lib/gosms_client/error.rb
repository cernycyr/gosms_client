module GosmsClient
  class Error < StandardError

    attr_reader :detail, :reason

    def initialize(reason, detail = nil)
      @reason = (reason.respond_to?(:to_s) && !reason.nil?) ? reason : :general
      @detail = detail || default_detail

      super(error_msg)
    end

    private

    def default_detail
      case @reason
      when :invalid_config
        return "Can't initialize client. Check if client_id, client_secret and channel_id is set. "
      else
        return nil
      end
    end

    def error_msg
      res = @reason.to_s
      res += ": " + @detail.to_s if @detail
      return res
    end

  end

  # Handles errors from OAuth2 gem communiation
  class HTTPError < Error

    attr_reader :status, :errors, :title

    def initialize(response)
      body = response.parsed || repsonse.body
      if body.is_a?(Hash)
        @status = body['status']
        @title = body['title']
        @errors = body['errors']
        @detail = body['detail'] || body
      else
        @detail = body
      end

      super(@title || :go_sms, error_details)
    end

    private

    def error_details
      res = @detail.to_s
      res += ". Errors: " + @errors.to_s if @errors
      return res
    end

  end
end
